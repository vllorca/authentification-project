const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const service = require('feathers-knex')
const knex = require('knex')
const socketio = require('@feathersjs/socketio')
const path = require('path')

const mailService = require("./services/mailService")

let jwt = require('jsonwebtoken')


let config = require('./knexfile.js')
const { response } = require('@feathersjs/express')
const { nextTick } = require('process')
const { readSync } = require('fs')
let database = knex(config.development)

// Create a feathers instance.
const app = express(feathers())

// Create HTML.pug
app.set("views", path.join(__dirname, "views"))
app.set("view engine", "pug")

// Content-Type: application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }))

// Content-Type: application/json
app.use(express.json())

// Content-Type: html/text
app.use(express.text())

// Transport: HTTP & websocket
app.configure(express.rest())
app.configure(socketio())

// Custom services
app.configure(mailService)

// PUG génération
app.use(express.static(path.join(__dirname, "public")))

// Feathers REST services
let userService = service({ Model: database, name: 'users', paginate: false })

// App GET

app.get("/", (req, res) => {
  res.render("index", {
    name: "Vivien",
    signupURL: "/signup",
    signinURL: "/signin"
  })
})

app.get("/signup", (req, res) => {
  res.render('signup', { title: 'Sign Up', message: 'Hello there - SIGN UP!' });
})

app.get("/signin", (req, res) => {
  res.render('signin', { title: 'Sign In', message: 'Hello there - SIGN IN!' });
})


app.get('/confirm-registration', (req, res, next) => {
  res.render('confirm-registration', { title: 'Confirm registration', message: 'Hello there - Confirm your registration here' });
})

app.get('/login', (req, res, next) => {
  res.render('login', { title: 'Login', message: 'Hello there - connect here' });
})

app.get('/home-page', (req, res, next) => {
  res.render('home-page', { title: 'HomePgae', message: 'Home Page' });
})

// App POST

app.post('/create-account', async (req, res, next) => {
  console.log("Data insertion start")

  let fullname = req.body.fullname;
  let email = req.body.email;

  let emailUsers = await userService.find({
    query: {
      email,
    }
  })

  if (emailUsers.length != 0) {
    res.send("error-user-already-exist")
  } else {
    await userService.create({ fullname, email })
  }

  let token = jwt.sign({ email: email }, "MYSECRET")

  const lien_validation = "http://localhost:3000/confirm-registration?" + token;

  await app.service("/mail-service").create({
    from: 'nasa.gouv@officiel.fr',
    to: email,
    subject: 'Email de validation compte',
    content: `
    Lien d'activation :
    <a href = ${lien_validation}>Send Email</a>
    `
  })
  res.send(`Le mail de validation a été envoyé à l'adresse ${email}`)
})

app.post('/activate-account', async (req, res, next) => {
  let email = req.body.email;
  let password = req.body.password

  let user = await userService.find(email)

  let passwordToken = jwt.sign({ password }, "MYSECRET")

  await userService.patch(user[0].id, {
    password: passwordToken,
    is_active: true
  })
  console.log(passwordToken)
  res.redirect('/login')
})

app.post('/login', async (req, res, next) => {
  let loginEmail = req.body.email;
  let loginPassword = req.body.password;

  let activeUsers = await userService.find({
    query: {
      email : loginEmail,
      is_active: true,
    }
  })

  if (activeUsers.length == 0) {
    res.send("error : user doesn't exist or account not activated")
  } else {
    let payload = jwt.decode(activeUsers[0].password)

    if (payload.password !== loginPassword) {
      res.send('Mot de passe invalide')
    } else {
      //Créer token de connection pour 2 heure
      res.redirect('/home-page')
    } 
  }
})

app.use('/users', userService)

// Start the server.
const port = process.env.PORT || 3000

app.listen(port, async () => {
  console.log(`Server listening on port ${port}`)
})