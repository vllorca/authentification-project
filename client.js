const io = require('socket.io-client')
const feathers = require('@feathersjs/feathers')
const socketio = require('@feathersjs/socketio-client')

const socket = io('http://localhost:3000')

const app = feathers()

app.configure(socketio(socket))


async function main() {
   let user = await app.service('/api/users').get(2)

   await app.service('mail-service').create({
      from: 'contact@nasa.gov',
      to: user.email,
      subject: 'Yo!',
      content: "Hello world!",
   })
   
   process.exit(0)
}

main()


